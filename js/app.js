import Router from "./paramHashRouter.js";
import Routes from "./routes.js";
import Reviews from "./reviews.js";

// window.router = new Router(Routes, "home");
window.router = new Router(Routes, "articles");
window.reviews = new Reviews("review-form", localStorage.devOpsComments);