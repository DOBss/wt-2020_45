
export default class Reviews {
    constructor(form, storage) {
        this.storage = storage;

        document.addEventListener("submit", event => this.processFormData(event));
    }


    processFormData(event) {
        let reviewForm = event.target;
        if (event.target.id !== "review-form") {
            return;
        }

        event.preventDefault();

        const reviewName = document.getElementById("review-name").value.trim();
        const reviewEmail = document.getElementById("review-email").value.trim();
        const reviewImage = document.getElementById("review-image").value.trim();
        const reviewKeywords = document.getElementById("review-keywords").value.trim();
        const reviewProfessional = reviewForm.elements['professional'].value;
        const reviewProfession = reviewForm.elements['profession'].value;
        const reviewText = document.getElementById("review-text").value.trim();

        if (reviewName === "" || reviewText === "") {
            window.alert("Please, enter both your name, email and text.");
            return;
        }

        if (!document.getElementById('terms-and-conditions').checked) {
            window.alert("You have to agree with terms and conditions.");
            return;
        }

        const newReview =
            {
                name: reviewName,
                email: reviewEmail,
                image: reviewImage,
                keywords: reviewKeywords,
                professional: reviewProfessional,
                profession: reviewProfession,
                text: reviewText,
                created: new Date()
            };

        console.info("New review:\n " + JSON.stringify(newReview));

        let reviews = this.storage ? JSON.parse(this.storage) : [];

        reviews.push(newReview);

        this.storage = JSON.stringify(reviews);

        document.getElementById("review-alert").classList.remove('hidden');
        console.info("New review added");
        console.log(reviews);

        reviewForm.reset();

        setTimeout(function () {
            document.getElementById("review-alert").classList.add('hidden');
        }, 2000);
    }
}