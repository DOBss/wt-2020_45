export default [
    {
        hash: "home",
        getTemplate: (targetElm) =>
            document.getElementById(targetElm).innerHTML = document.getElementById("template-home").innerHTML
    },

    {
        hash: "articles",
        getTemplate: fetchAndDisplayArticles
    },

    {
        hash: "opinions",
        getTemplate: createHtml4opinions
    },

    {
        hash: "addOpinion",
        getTemplate: (targetElm) =>
            document.getElementById(targetElm).innerHTML = document.getElementById("template-addOpinion").innerHTML
    }
];

function fetchAndDisplayArticles(targetElm) {
    const url = "https://wt.kpi.fei.tuke.sk/api/article";
    let articleList = [];

    fetch(url)
        .then(response => {
            if (response.ok) {
                return response.json();
            } else { //if we get server error
                return Promise.reject(new Error(`Server answered with ${response.status}: ${response.statusText}.`));
            }
        })
        .then(responseJSON => {
            articleList = responseJSON.articles;
            return Promise.resolve();
        })
        .then(() => {
            let cntRequests = articleList.map(
                article => fetch(`${url}/${article.id}`)
            );
            return Promise.all(cntRequests);
        })
        .then(responses => {
            let failed = "";
            for (let response of responses) {
                if (!response.ok) failed += response.url + " ";
            }
            if (failed === "") {
                return responses;
            } else {
                return Promise.reject(new Error(`Failed to access the content of the articles with urls ${failed}.`));
            }
        })
        .then(responses => Promise.all(responses.map(resp => resp.json())))
        .then(articles => {
            articles.forEach((article, index) => {
                articleList[index].content = article.content;
            });
            return Promise.resolve();
        })
        .then(() => {
            console.log(articleList);
            document.getElementById(targetElm).innerHTML =
                Mustache.render(
                    document.getElementById("template-articles").innerHTML,
                    articleList
                );

        })
        .catch(error => { ////here we process all the failed promises
            const errMsgObj = {errMessage: error};
            document.getElementById(targetElm).innerHTML =
                Mustache.render(
                    document.getElementById("template-articles-error").innerHTML,
                    errMsgObj
                );
        });

}

function createHtml4opinions(targetElm) {
    const opinionsFromStorage = localStorage.devOpsComments;
    let opinions = [];

    if (opinionsFromStorage) {
        opinions = JSON.parse(opinionsFromStorage);
        console.log(opinions);
        opinions.forEach(opinion => {
            opinion.hasImage = opinion.image !== "";
            opinion.profession = {
                dev: "developer",
                devops: "DevOps engineer",
            }[opinion.profession] || (opinion.professional === "yes" ? "IT professional" : "enthusiast");
            opinion.created = (new Date(opinion.created)).toDateString();
        });
    }

    document.getElementById(targetElm).innerHTML = Mustache.render(
        document.getElementById("template-opinions").innerHTML,
        opinions
    );
}

function createHtml4Main(targetElm, current, totalCount) {

    current = parseInt(current);
    totalCount = parseInt(totalCount);
    const data4rendering = {
        currPage: current,
        pageCount: totalCount
    };


    if (current > 1) {
        data4rendering.prevPage = current - 1;
    }

    if (current < totalCount) {
        data4rendering.nextPage = current + 1;
    }

    document.getElementById(targetElm).innerHTML = Mustache.render(
        document.getElementById("template-main").innerHTML,
        data4rendering
    );


    /*
        return `<h1>Main Content</h1>
                ${current} <br>
                ${totalCount} <br>
                ${JSON.stringify(data4rendering)}
                `;
    */
}
